// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
// Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app"
// import { getAnalytics } from "firebase/analytics"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDg63qvOa79u5sVxJdHsBbCBAyG4c57yHo",
  authDomain: "frontfake-210aa.firebaseapp.com",
  projectId: "frontfake-210aa",
  storageBucket: "frontfake-210aa.appspot.com",
  messagingSenderId: "285806498597",
  appId: "1:285806498597:web:08f8505a79fa8bca1580cc",
  measurementId: "G-5WL9CYEFDR"
};

// Initialize Firebase
// const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);